﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace AppliedMathsAssessment
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        int displaywidth = 1080;
        int displayheight = 720;
        Camera gamecam;
        DirectionalLightSource sunlight;
        PopUp winPopup, losePopup;
        bool gameEnded = false;
        
        private List<PhysicsObject> gameObjects = new List<PhysicsObject>();
        private List<PhysicsObject> toAdd = new List<PhysicsObject>();
        private List<PhysicsObject> toRemove = new List<PhysicsObject>();


        private SoundEffect gameWinSFX;
        private SoundEffect gameLoseSFX;

        public void AddObject(PhysicsObject newObject)
        {
            toAdd.Add(newObject);
        }

        public void RemoveObject(PhysicsObject newObject)
        {
            toRemove.Add(newObject);
        }

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            graphics.PreferredBackBufferWidth = displaywidth;
            graphics.PreferredBackBufferHeight = displayheight;
            graphics.ApplyChanges();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Show mouse
            this.IsMouseVisible = true;

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Camera setup
            gamecam.aspectRatio = (float)displaywidth / (float)displayheight;
            gamecam.offset = new Vector3(0, 50, -50);
            gamecam.fieldOfView = MathHelper.ToRadians(90);
            gamecam.whichWayIsUp = Vector3.Up;
            gamecam.nearPlane = 1f;
            gamecam.farPlane = 50000f;

            // Lighting setup
            sunlight.diffuseColor = new Vector3(10);
            sunlight.specularColor = new Vector3(1f, 1f, 1f);
            sunlight.direction = Vector3.Normalize(new Vector3(1.5f, -1.5f, -1.5f));

            // Creat popups
            Texture2D popupTexture = Content.Load<Texture2D>("Panel");
            Texture2D buttonTexture = Content.Load<Texture2D>("Button");
            Texture2D pressedTexture = Content.Load<Texture2D>("ButtonPressed");
            SpriteFont buttonFont = Content.Load<SpriteFont>("mainFont");
            SoundEffect clickSFX = Content.Load<SoundEffect>("Click");
            SpriteFont panelFont = Content.Load<SpriteFont>("largeFont");
            winPopup = new PopUp(popupTexture,
                buttonTexture,
                pressedTexture,
                buttonFont,
                clickSFX,
                buttonFont,
                "YOU WIN",
                "Restart",
                GraphicsDevice);
            losePopup = new PopUp(popupTexture,
                buttonTexture,
                pressedTexture,
                buttonFont,
                clickSFX,
                buttonFont,
                "YOU LOSE",
                "Restart",
                GraphicsDevice);

            // Create all other objects
            SetupObjects();

            // Load win/loss jingles
            gameWinSFX = Content.Load<SoundEffect>("WinJingle");
            gameLoseSFX = Content.Load<SoundEffect>("GameOverJingle");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (gameEnded == false)
            {
                // Update all game objects
                foreach (PhysicsObject gameObject in gameObjects)
                {
                    gameObject.Update(gameTime);
                }

                // Handle collisions
                for (int i = 0; i < gameObjects.Count; ++i)
                {
                    PhysicsObject thisObject = gameObjects[i];

                    // Start inner loop at the next object - all previous will already have been checked
                    for (int j = i + 1; j < gameObjects.Count; ++j)
                    {
                        PhysicsObject otherObject = gameObjects[j];

                        if (thisObject.GetHitBox().Intersects(otherObject.GetHitBox()))
                        {

                            thisObject.HandleCollision(otherObject);
                            otherObject.HandleCollision(thisObject);
                        }
                    }
                }

                // Add or remove GameObjects
                foreach (PhysicsObject newObject in toAdd)
                {
                    gameObjects.Add(newObject);
                }
                toAdd.Clear();
                foreach (PhysicsObject removeObject in toRemove)
                {
                    gameObjects.Remove(removeObject);
                }
                toRemove.Clear();

                // Check for enemies
                int numEnemies = 0;
                foreach (PhysicsObject gameObject in gameObjects)
                {
                    if (gameObject is Enemy)
                        ++numEnemies;
                }
                if (numEnemies == 0)
                {
                    WinGame();
                }
            }
            else
            {
                if (winPopup.IsClicked() || losePopup.IsClicked())
                    RestartGame();
            }
            
            // Update win/lose
            winPopup.Update(gameTime);
            losePopup.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            
            graphics.GraphicsDevice.BlendState = BlendState.Opaque; // set up 3d rendering so its not transparent
            graphics.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            // Draw 3D objects
            foreach (PhysicsObject gameObject in gameObjects)
            {
                gameObject.Draw(gamecam, sunlight);
            }

            // Draw UI
            spriteBatch.Begin();

            winPopup.Draw(spriteBatch);
            losePopup.Draw(spriteBatch);

            spriteBatch.End();

            base.Draw(gameTime);
        }

        public void WinGame()
        {
            if (!gameEnded)
            {
                gameWinSFX.Play();
                gameEnded = true;
                winPopup.Show();

                // Stop the tank sfx
            }
        }

        public void LoseGame()
        {
            if (!gameEnded)
            {
                gameLoseSFX.Play();
                gameEnded = true;
                losePopup.Show();
            }
        }

        public void RestartGame()
        {
            gameEnded = false;
            losePopup.Hide();
            winPopup.Hide();
            gameObjects.Clear();
            SetupObjects();
        }

        private void SetupObjects()
        {
            // Bullet setup
            Bullet bullet = new Bullet();
            bullet.LoadModel(Content, "Rocket", GraphicsDevice);
            SoundEffect cannonHit = Content.Load<SoundEffect>("CannonHit");
            SoundEffect enemyExplode = Content.Load<SoundEffect>("EnemyExplode");
            bullet.Initialise(this, cannonHit, enemyExplode);

            // Pip setup
            ModelObject pip = new ModelObject();
            pip.LoadModel(Content, "Ball", GraphicsDevice);
            pip.SetAlpha(0.2f);

            // Player setup
            Player player = new Player();
            player.LoadModel(Content, "Tank", GraphicsDevice);
            SoundEffect wallBounce = Content.Load<SoundEffect>("WallBounce");
            SoundEffect cannonFire = Content.Load<SoundEffect>("CannonFire");
            SoundEffect tankMove = Content.Load<SoundEffect>("TankMove");
            player.Initialise(bullet, pip, this, wallBounce, cannonFire, tankMove);
            gamecam.targetObject = player;
            gameObjects.Add(player);

            // Enemy setup
            CreateEnemy(new Vector3(200, 0, 200), player);
            CreateEnemy(new Vector3(0, 0, 300), player);
            CreateEnemy(new Vector3(-200, 0, 200), player);

            // Setup walls
            BasicCuboid[] walls = new BasicCuboid[4];
            float wallLength = 1000f;
            float halfWallLength = wallLength * 0.5f;
            for (int c = 0; c < walls.Length; c++)
            {
                walls[c] = new BasicCuboid(GraphicsDevice);
                walls[c].LoadContent(Content, "WallTexture");
            }
            walls[0].SetUpVertices(new Vector3(halfWallLength, -1, -halfWallLength), new Vector3(5, 30, wallLength));
            walls[1].SetUpVertices(new Vector3(-halfWallLength, -1, halfWallLength), new Vector3(wallLength, 30, 5));
            walls[2].SetUpVertices(new Vector3(-halfWallLength, -1, -halfWallLength), new Vector3(5, 30, wallLength));
            walls[3].SetUpVertices(new Vector3(-halfWallLength, -1, -halfWallLength), new Vector3(wallLength, 30, 5));
            foreach (BasicCuboid wall in walls)
            {
                gameObjects.Add(wall);
            }

            // Setup Floor
            BasicCuboid floor = new BasicCuboid(GraphicsDevice);
            floor.LoadContent(Content, "FloorTexture");
            floor.SetUpVertices(new Vector3(-halfWallLength, -6, -halfWallLength), new Vector3(wallLength, 5, wallLength));
            gameObjects.Add(floor);
        }

        private void CreateEnemy(Vector3 enemyPos, Player player)
        {
            Enemy enemy = new Enemy();
            enemy.LoadModel(Content, "Enemy", GraphicsDevice);
            enemy.Initialise(enemyPos, player);
            gameObjects.Add(enemy);
        }
    }
}
