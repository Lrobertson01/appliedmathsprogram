﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace AppliedMathsAssessment
{
    public class PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Transform
        protected Vector3 rotation;
        protected Vector3 position;
        protected Vector3 scale = Vector3.One;

        // Physics
        protected BoundingBox hitBox;
        protected Vector3 velocity;
        protected Vector3 acceleration;
        protected float drag = 0.05f;
        protected Vector3 collisionScale = Vector3.One;
        protected bool isStatic = false;    // Not moved by physics
        protected bool useGravity = false;  // Falls with gravity each frame
        protected bool isTrigger = true;    // Does not trigger physics affects (but can still be sensed with collisions)
        protected float gravityScale = 1f;

        // Previous state
        protected Vector3 positionPrev;
        protected Vector3 velocityPrev;
        protected Vector3 accelerationPrev;

        // Numerical Integration
        public enum IntegrationMethod {
            EXPLICIT_EULER,
            Implicit_Euler,
            Semi_Implicit_Euler
        };
        // UPDATE THIS TO TEST TASK 6
        private IntegrationMethod currentIntegrationMethod = IntegrationMethod.Semi_Implicit_Euler;

        // ------------------
        // Behaviour
        // ------------------
        public BoundingBox GetHitBox()
        {
            return hitBox;
        }
        // ------------------
        public virtual void UpdateHitBox()
        {
            // Just make a cube hitbox based on the scale
            hitBox = new BoundingBox(-collisionScale * 0.5f, collisionScale * 0.5f);

            // Move to correct position in game world
            hitBox.Min += position;
            hitBox.Max += position;
        }
        // ------------------
        public Vector3 GetPosition()
        {
            return position;
        }
        // ------------------
        public void SetPosition(Vector3 newPosition)
        {
            position = newPosition;
        }
        // ------------------
        public void SetScale(Vector3 newScale)
        {
            scale = newScale;
        }
        // ------------------
        public Vector3 GetGravityVector()
        {
            return new Vector3(0, -9.8f * gravityScale, 0);
        }
        // ------------------
        public virtual void Update(GameTime gameTime)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update acceleration due to gravity
            if (useGravity)
                acceleration.Y = -9.8f * gravityScale;

            // Store current state before making any modifications
            Vector3 positionCur = position;
            Vector3 velocityCur = velocity;
            Vector3 accelerationCur = acceleration;

            // Update velocity due to drag
            velocity *= (1.0f - drag);

            // Update velocity and position based on acceleration
            // Uses numerical integration (multiple possible methods)
            switch (currentIntegrationMethod)
            {
                case IntegrationMethod.EXPLICIT_EULER:
                    // This method is being deprecated due to stability issues.
                    position += velocity * dt;
                    velocity += acceleration * dt;
                    break;
                    
                ///////////////////////////////////////////////////////////////////
                //
                // CODE FOR TASK 6 SHOULD BE ENTERED HERE
                //
                ///////////////////////////////////////////////////////////////////
                case IntegrationMethod.Implicit_Euler:

                    velocity += acceleration * dt;
                    position += velocity * dt;
                    break;

                case IntegrationMethod.Semi_Implicit_Euler:

                    velocity += (accelerationPrev * dt);
                    position += velocity * dt;

                    break;

                ///////////////////////////////////////////////////////////////////  
                // END TASK 6 CODE
                ///////////////////////////////////////////////////////////////////  
            }

            // Store current state as previous state
            positionPrev = positionCur;
            velocityPrev = velocityCur;
            accelerationPrev = accelerationCur;

            // Update hitbox
            UpdateHitBox();
        }
        // ------------------
        public virtual void HandleCollision(PhysicsObject other)
        {
            // Don't react with physics if this object is static
            if (isStatic || isTrigger || other.isTrigger)
                return;

            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 3 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

                if (other.GetHitBox().Intersects(GetHitBox()))
                {
                   
                    //get our bounding boxes for ease

                    BoundingBox thisHitBox = GetHitBox();
                    BoundingBox thatHitBox = other.GetHitBox();


                    //calculate collision depth

                    Vector3 CentreOther = thatHitBox.Min + (thatHitBox.Max - thatHitBox.Min);
                    Vector3 CentreThis = thisHitBox.Min + (thisHitBox.Max - thisHitBox.Min);

                    Vector3 distance = CentreOther - CentreThis;

                    Vector3 minDistance = (thatHitBox.Max - thatHitBox.Min) / 2.0f + (thisHitBox.Max - thisHitBox.Min) / 2.0f;

                    if (distance.X < 0)
                    { minDistance.X = -minDistance.X; }
                    if (distance.Y < 0)
                    { minDistance.Y = -minDistance.Y; }
                    if (distance.Z < 0)
                    { minDistance.Z = -minDistance.Z; }

                    Vector3 depth = minDistance - distance;


                    Vector3[] corners = thatHitBox.GetCorners();


                    Vector3 planeVector1;
                    Vector3 planeVector2;

                    if (Math.Abs(depth.Z) > Math.Abs(depth.X))
                    {
                        planeVector1 = corners[0] - corners[5];
                        planeVector2 = corners[6] - corners[5];
                    }
                    else
                    {
                        planeVector1 = corners[1] - corners[0];
                        planeVector2 = corners[3] - corners[0];
                    }

                    Vector3 normal = Vector3.Cross(planeVector1, planeVector2);

                    normal.Normalize();

                    velocity = Vector3.Reflect(velocity, normal);
                }
            


            ///////////////////////////////////////////////////////////////////  
            // END TASK 3 CODE
            ///////////////////////////////////////////////////////////////////  
        }
        // ------------------
        public virtual void Draw(Camera cam, DirectionalLightSource light)
        {

        }
        // ------------------
    }
}
