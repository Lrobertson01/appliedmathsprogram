﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace AppliedMathsAssessment
{
    class ModelObject : PhysicsObject
    {
        // ------------------
        // Data
        // ------------------

        // Rendering
        private Matrix[] transforms;
        private Model mesh;
        private GraphicsDevice graphicsDevice;
        private VertexBuffer cubeVertexBuffer;
        private BasicEffect hitBoxEffect;
        protected bool visible = false;
        protected float alpha = 1;
        protected bool drawHitBox = true;


        // ------------------
        // Behaviour
        // ------------------
        public void LoadModel(ContentManager content, string modelName, GraphicsDevice newGraphics)
        {
            mesh = content.Load<Model>(modelName);
            transforms = new Matrix[mesh.Bones.Count];
            mesh.CopyAbsoluteBoneTransformsTo(transforms);
            visible = true;

            // Setup for bounding box rendering
            graphicsDevice = newGraphics;
            int[] boxpos = new int[] { 0, 0, 0, 0, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1,
0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0,
0, 1, 0 };
            VertexPositionColor[] boxVerts = new VertexPositionColor[17];
            for (int index = 0; index < boxpos.Length; index += 3)
                boxVerts[index / 3] = new VertexPositionColor(new
                Vector3(boxpos[index], boxpos[index + 1], boxpos[index + 2]), Color.White);
            cubeVertexBuffer = new VertexBuffer(graphicsDevice,
            typeof(VertexPositionColor), boxVerts.Length, BufferUsage.None);
            cubeVertexBuffer.SetData(boxVerts);
            hitBoxEffect = new BasicEffect(graphicsDevice);
        }
        // ------------------
        public void LoadModel(Model model)
        {
            mesh = model;
            transforms = new Matrix[mesh.Bones.Count];
            mesh.CopyAbsoluteBoneTransformsTo(transforms);
            visible = true;
        }
        // ------------------
        public void CopyTo(ModelObject newObject)
        {
            newObject.mesh = mesh;
            newObject.transforms = transforms;
            newObject.graphicsDevice = graphicsDevice;
            newObject.cubeVertexBuffer = cubeVertexBuffer;
            newObject.hitBoxEffect = hitBoxEffect;
            newObject.visible = visible;
            newObject.alpha = alpha;
            newObject.drawHitBox = drawHitBox;
        }
        // ------------------
        public void SetAlpha(float newAlpha)
        {
            alpha = newAlpha;
        }
        // ------------------
        public override void Draw(Camera cam, DirectionalLightSource light)
        {
            if (!visible) return; // dont render hidden meshes
            
            foreach (ModelMesh mesh in mesh.Meshes) // loop through the mesh in the 3d model, drawing each one in turn.
            {
                foreach (BasicEffect effect in mesh.Effects) // This loop then goes through every effect in each mesh.
                {
                    effect.World = transforms[mesh.ParentBone.Index]; // begin dealing with transforms to render the object into the game world
                                                                      // The following effects allow the object to be drawn in the correct place, with the correct rotation and scale.

                    ///////////////////////////////////////////////////////////////////
                    //
                    // CODE FOR TASK 1 SHOULD BE ENTERED HERE
                    //
                    ///////////////////////////////////////////////////////////////////  

                    // Translate / Rotate / Scale the model into the correct area of the game world
                    //Scale
                    effect.World *= Matrix.CreateScale(scale);
                    //Rotation
                    effect.World *= Matrix.CreateRotationX(rotation.X);
                    effect.World *= Matrix.CreateRotationY(rotation.Y);
                    effect.World *= Matrix.CreateRotationZ(rotation.Z);
                    
                    effect.World *= Matrix.CreateTranslation(position);

                    // Transform it relative to the camera's view

                    effect.View = Matrix.CreateLookAt(cam.position, cam.targetObject.GetPosition(), cam.whichWayIsUp);


                    // Project it onto the 2D surface of the game viewport

                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(cam.fieldOfView, cam.aspectRatio, cam.nearPlane, cam.farPlane);

                    ///////////////////////////////////////////////////////////////////  
                    // END TASK 1 CODE
                    ///////////////////////////////////////////////////////////////////  

                    // the following effects are related to lighting and texture  settings, feel free to tweak them to see what happens.
                    effect.LightingEnabled = true;
                    effect.Alpha = alpha; //  amount of transparency
                    effect.AmbientLightColor = new Vector3(0.25f); // fills in dark areas with a small amount of light
                    effect.DiffuseColor = new Vector3(0.1f);
                    // Diffuse is the standard colour method
                    effect.DirectionalLight0.Enabled = true; // allows a directional light
                    effect.DirectionalLight0.DiffuseColor = light.diffuseColor; // the directional light's main colour
                    effect.DirectionalLight0.SpecularColor = light.specularColor; // the directional light's colour used for highlights
                    effect.DirectionalLight0.Direction = light.direction; // the direction of the light
                    effect.EmissiveColor = new Vector3(0.15f);
                }
                mesh.Draw(); // draw the current mesh using the effects.
            }

            if (drawHitBox)
                DrawBoundingBox(cam);
        }
        // ------------------
        public void DrawBoundingBox(Camera cam)
        {
            hitBoxEffect.LightingEnabled = false;
            hitBoxEffect.VertexColorEnabled = false;
            BoundingBox box = GetHitBox();
            Color wireColour = Color.Black; // TODO - change to white when colliding


            graphicsDevice.SetVertexBuffer(cubeVertexBuffer);
            hitBoxEffect.World =
            Matrix.CreateScale(box.Max - box.Min) *
            Matrix.CreateTranslation(box.Min);
            hitBoxEffect.View = Matrix.CreateLookAt(cam.position, cam.target, cam.whichWayIsUp);
            hitBoxEffect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        cam.fieldOfView, cam.aspectRatio, cam.nearPlane, cam.farPlane);
            hitBoxEffect.DiffuseColor = wireColour.ToVector3();
            foreach (EffectPass pass in hitBoxEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                graphicsDevice.DrawPrimitives(PrimitiveType.LineStrip, 0, 16);
            }
        }
        // ------------------
        public override void UpdateHitBox()
        {
            ///////////////////////////////////////////////////////////////////
            //
            // CODE FOR TASK 2 SHOULD BE ENTERED HERE
            //
            ///////////////////////////////////////////////////////////////////

            // Just make a cube hitbox based on the scale
            BoundingBox Bounds = new BoundingBox();

            Vector3 collisionOffset = Vector3.Zero;

            foreach (ModelMesh Mesh in mesh.Meshes)
            {
                foreach (ModelMeshPart meshPart in Mesh.MeshParts)
                {
                    VertexPositionNormalTexture[] modelVertices = new VertexPositionNormalTexture[meshPart.VertexBuffer.VertexCount];

                    meshPart.VertexBuffer.GetData(modelVertices);

                    Vector3[] vertices = new Vector3[modelVertices.Length];

                    //get the bone transforms and apply TRS transforms
                    Matrix meshTransform = Mesh.ParentBone.Transform;

                    meshTransform *= Matrix.CreateScale(scale);
                    meshTransform *= Matrix.CreateScale(collisionScale);
                    meshTransform *= Matrix.CreateRotationX(rotation.X);
                    meshTransform *= Matrix.CreateRotationY(rotation.Y);
                    meshTransform *= Matrix.CreateRotationZ(rotation.Z);

                    for (int i = 0; i < vertices.Length; ++i)
                    {
                        //Grab position of the vertex
                        //And then transform it using the bone transform we just made

                        vertices[i] = Vector3.Transform(modelVertices[i].Position, meshTransform);
                    }

                    //Create an AABB from the models vertices 

                    Bounds = BoundingBox.CreateMerged(Bounds, BoundingBox.CreateFromPoints(vertices));
                }

                Bounds.Min += position + collisionOffset + mesh.Meshes[0].BoundingSphere.Center;
                Bounds.Max += position + collisionOffset + mesh.Meshes[0].BoundingSphere.Center;
            }


            hitBox = Bounds;

            ///////////////////////////////////////////////////////////////////  
            // END TASK 2 CODE
            ///////////////////////////////////////////////////////////////////
        }



    }


}
